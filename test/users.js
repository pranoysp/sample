/**
 * Created by AMT
 */
var assert = require('assert');
var usersDomain = require('../domain/users');

describe('UsersTests', function() {

    it('Try to add user', function(done) {
        var TEST_USER_DATA = {
            "username": "test",
            "password": "test123"
        };
        usersDomain.createUser(TEST_USER_DATA)
            .then(user => {
                assert(user.username == TEST_USER_DATA.username);
                assert(user.password == TEST_USER_DATA.password);
                done();
            })
            .catch(err => {
                console.log('user testcase--', err);
            });
    });   

});