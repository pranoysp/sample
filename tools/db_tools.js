/**
 * Created by AMT
 */
'use strict';

var mongoose = require('mongoose');
var config = require('../config/config.json');
var Promise = mongoose.Promise;//require('es6-promise').Promise;

var db;

exports.DBConnectMongoose = function() {
    return new Promise(function(resolve, reject) {
        if (db) {
            return db;
        }
        mongoose.Promise = global.Promise;

        // database connect
		var mongo_url = config.db_config.host + ":" + config.db_config.port + "/" + config.db_config.name;
        mongoose.connect('mongodb://' + mongo_url)
            .then(() => {
                console.log('mongo connection created');
                resolve(db);
            })
            .catch(err => {
                console.log('error creating db connection: ' + err);
                reject(db);
            });
    });
};