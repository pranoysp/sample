/**
 * Created by AMT
 */
var users = require('../controllers/users');


exports.assignRoutes = function (app) {
    app.post('/users', users.createUser);
};