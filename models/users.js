/**
 * Created by AMT.
 */

var db_tools = require('../tools/db_tools');
var mongoose = require('mongoose');

// database connect
var db = db_tools.DBConnectMongoose();

// Create a Mongoose schema
var UserSchema = new mongoose.Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    status: {type: Boolean, default: true}
});

// Register the schema
var User = mongoose.model('user', UserSchema);

exports.User = User;
exports.saveUser = function(userData) {
    var user = new User(userData);
    return new Promise(function(resolve, reject) {
        user.save()
            .then(user => {
                console.log("User saved!");
                resolve(user);
            })
            .catch(err => {
                console.log("Error saving user: " + err);
                reject(err);
            });
    });
};
