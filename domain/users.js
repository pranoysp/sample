/**
 * Created by AMT
 */
var usersDB = require('../models/users');

exports.createUser = function(userData) {
    return new Promise(function(resolve, reject) {
        if (!userData.username ||
            !userData.password) {
            reject('Missing fields');
            return;
        }
        
        usersDB.saveUser(userData)
            .then(user => {
                resolve(user);
            })
            .catch(err => {
                reject(err);
            });
    });
};