/**
 * Created by AMT
 */
var usersDomain = require('../domain/users');
var jwt = require('jwt-simple');

exports.createUser = function (req, res, next) {
    var userData = req.body;
	var resObj;
	getEncryptedPassword = function(password, salt) {
		salt = '';
		salt = salt.replace('$', '');
		var sum = require('crypto').createHash('sha512').update(password + salt).digest('hex');
		return "$6$" + salt + "$" + sum;
    };
	var salt = "";
	userData.password = getEncryptedPassword(userData.password, salt);
    usersDomain.createUser(userData)
        .then(user => {
			resObj = {
				status: true,
				"response": {
					"message": "Registered Successfully",
					"user_id": user._id,
					"full_name": user.full_name,
					"access_token": genToken(user)
				}
			};
            res.send(resObj);
        })
        .catch(err => {
			resObj = {
				status: false,
				"response": {
					"message": "Registration failed"
				}
			};
            res.status(400).send(resObj);
        });
};

function genToken(user) {
	console.log('user--', user);
    var expires = expiresIn(30); // 30 days
    var token = jwt.encode({
        exp: expires,
        user_id: user._id
    }, require('../config/secret')());
	console.log('token--', token);
    return token;
}

function expiresIn(numDays) {
    var dateObj = new Date();

    return dateObj.setDate(dateObj.getDate() + numDays);
}