/**
 * Created by AMT
 */
(function () {
	'use strict';

	var express = require('express');
	var bodyparser = require('body-parser');
	var env = process.env.NODE_ENV || 'local';
	var db_tools = require('./tools/db_tools');
	
	var app = express();
	app.all('/*', function(req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
		res.header('Access-Control-Allow-Headers', 'Content-type,Accept,access-token,X-Key');
		if (req.method == 'OPTIONS') {
		res.status(200).end();
		} else {
		next();
		}
	});

	db_tools.DBConnectMongoose()
		.then(() => {
			var routes = require('./routes/routes');

			// configure app to use bodyParser()
			// this will let us get the data from a POST
			app.use(bodyparser.urlencoded({extended: true}));
			app.use(bodyparser.json({limit: '10mb'}));

			routes.assignRoutes(app);

			app.listen(6025);

			console.log('Server listening on port 3000');
		})
		.catch(err => {
			console.log('Error: ' + err);
	});
}());